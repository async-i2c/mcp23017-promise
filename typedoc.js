module.exports = {
    "out": "doc",
    "exclude": "**/+(node_modules|__tests__)/**/*",
    "excludeExternals": true,
    "excludePrivate": true
}
