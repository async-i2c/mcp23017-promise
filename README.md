# MCP23017-promise
The MCP23017 is a 16-bit I/O expander with an I2C interface manufactured by Microchip.

MCP23017-promise is a node.js library written in TypeScript that offers a modern promise based interface to the usual commands of the chip.

The library does not handle the SPI interface of the MCP23S17.

Status: alpha software - work in progress.
Licence: BSD 3 clauses.
