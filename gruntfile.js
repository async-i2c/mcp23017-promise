module.exports = function (grunt) {
    'use strict';
    grunt.initConfig({
  
      ts: {
        default: {
          tsconfig: true
        }
      },
      mochaTest: {
        test: {
          options: {
            reporter: 'spec',
            require: 'ts-node/register'
          },
          src: ['tests/**/*.ts']
        }
      }
    });
  
    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.registerTask('default', ['ts']);
  
  };
  