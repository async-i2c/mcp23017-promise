module.exports = {
    transform: {
        '^.+\\.ts$': 'ts-jest',
    },
    testRegex: '/__tests__/.*\\.ts$',
    moduleFileExtensions: ['ts', 'js'],
}